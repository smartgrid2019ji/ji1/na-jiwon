package 전화번호부;

import java.awt.CardLayout;
import java.awt.Color;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.GridLayout;
import java.util.ArrayList;

import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.SpringLayout;
import javax.swing.SwingConstants;



import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;



public class PhoneGUI {
	PhoneDialog dialog;
	private JFrame frame;
	static PhoneGUI window;
	
static DefaultListModel model;
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					window = new PhoneGUI();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public PhoneGUI() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.getContentPane().setBackground(Color.WHITE);
		frame.setBackground(Color.WHITE);
		frame.setBounds(100, 100, 450, 450);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		SpringLayout springLayout = new SpringLayout();
		frame.getContentPane().setLayout(springLayout);
		
		JPanel panel = new JPanel();
		panel.setBackground(Color.DARK_GRAY);
		springLayout.putConstraint(SpringLayout.NORTH, panel, 10, SpringLayout.NORTH, frame.getContentPane());
		springLayout.putConstraint(SpringLayout.WEST, panel, 10, SpringLayout.WEST, frame.getContentPane());
		springLayout.putConstraint(SpringLayout.SOUTH, panel, 105, SpringLayout.NORTH, frame.getContentPane());
		springLayout.putConstraint(SpringLayout.EAST, panel, 422, SpringLayout.WEST, frame.getContentPane());
		frame.getContentPane().add(panel);
		
		JPanel panel_1 = new JPanel();
		springLayout.putConstraint(SpringLayout.NORTH, panel_1, 6, SpringLayout.SOUTH, panel);
		springLayout.putConstraint(SpringLayout.WEST, panel_1, 10, SpringLayout.WEST, frame.getContentPane());
		springLayout.putConstraint(SpringLayout.SOUTH, panel_1, 227, SpringLayout.SOUTH, panel);
		SpringLayout sl_panel = new SpringLayout();
		panel.setLayout(sl_panel);
		
		JLabel lbl_title = new JLabel("\uC804\uD654\uBAA9\uB85D");
		sl_panel.putConstraint(SpringLayout.WEST, lbl_title, 10, SpringLayout.WEST, panel);
		sl_panel.putConstraint(SpringLayout.SOUTH, lbl_title, -30, SpringLayout.SOUTH, panel);
		sl_panel.putConstraint(SpringLayout.EAST, lbl_title, 402, SpringLayout.WEST, panel);
		lbl_title.setHorizontalAlignment(SwingConstants.CENTER);
		lbl_title.setForeground(Color.WHITE);
		lbl_title.setFont(new Font("동그라미재단M", Font.BOLD, 28));
		panel.add(lbl_title);
		springLayout.putConstraint(SpringLayout.EAST, panel_1, 422, SpringLayout.WEST, frame.getContentPane());
		frame.getContentPane().add(panel_1);
		
		JPanel panel_2 = new JPanel();
		springLayout.putConstraint(SpringLayout.NORTH, panel_2, 6, SpringLayout.SOUTH, panel_1);
		springLayout.putConstraint(SpringLayout.WEST, panel_2, 10, SpringLayout.WEST, frame.getContentPane());
		springLayout.putConstraint(SpringLayout.SOUTH, panel_2, 63, SpringLayout.SOUTH, panel_1);
		panel_1.setLayout(new GridLayout(1, 0, 0, 0));
		
		JPanel panel_4 = new JPanel();
		panel_1.add(panel_4);
		panel_4.setLayout(new CardLayout(0, 0));
		
		model = new DefaultListModel<>();
		PhoneDAO dato = new PhoneDAO();
		ArrayList<String> showlist = dato.getlist();
		
		for (int i = 0; i < showlist.size(); i++) {
			model.addElement(showlist.get(i));
		}
	
		
		JList list = new JList(model);
		list.setLayoutOrientation(JList.VERTICAL_WRAP);
		list.setFont(new Font("동그라미재단M", Font.PLAIN, 22));
		panel_4.add(list, "name_1642170276855");
		
		JPanel panel_3 = new JPanel();
		panel_3.setBackground(Color.WHITE);
		panel_3.setForeground(Color.WHITE);
		panel_1.add(panel_3);
		SpringLayout sl_panel_3 = new SpringLayout();
		panel_3.setLayout(sl_panel_3);
		
		JLabel lbl_tname = new JLabel("\uC774\uB984   ");
		sl_panel_3.putConstraint(SpringLayout.NORTH, lbl_tname, 37, SpringLayout.NORTH, panel_3);
		sl_panel_3.putConstraint(SpringLayout.WEST, lbl_tname, 10, SpringLayout.WEST, panel_3);
		lbl_tname.setFont(new Font("동그라미재단M", Font.PLAIN, 20));
		panel_3.add(lbl_tname);
		
		JLabel lbl_tage = new JLabel("\uB098\uC774  ");
		sl_panel_3.putConstraint(SpringLayout.NORTH, lbl_tage, 33, SpringLayout.SOUTH, lbl_tname);
		sl_panel_3.putConstraint(SpringLayout.WEST, lbl_tage, 0, SpringLayout.WEST, lbl_tname);
		lbl_tage.setFont(new Font("동그라미재단M", Font.PLAIN, 20));
		panel_3.add(lbl_tage);
		
		JLabel lbl_ttel = new JLabel("\uC804\uD654\uBC88\uD638  ");
		sl_panel_3.putConstraint(SpringLayout.NORTH, lbl_ttel, 33, SpringLayout.SOUTH, lbl_tage);
		sl_panel_3.putConstraint(SpringLayout.EAST, lbl_tname, 0, SpringLayout.EAST, lbl_ttel);
		lbl_ttel.setFont(new Font("동그라미재단M", Font.PLAIN, 20));
		sl_panel_3.putConstraint(SpringLayout.WEST, lbl_ttel, 10, SpringLayout.WEST, panel_3);
		panel_3.add(lbl_ttel);
		
		JLabel lbl_name = new JLabel("-");
		sl_panel_3.putConstraint(SpringLayout.NORTH, lbl_name, 4, SpringLayout.NORTH, lbl_tname);
		sl_panel_3.putConstraint(SpringLayout.WEST, lbl_name, 6, SpringLayout.EAST, lbl_tname);
		sl_panel_3.putConstraint(SpringLayout.EAST, lbl_name, -15, SpringLayout.EAST, panel_3);
		lbl_name.setFont(new Font("동그라미재단M", Font.PLAIN, 15));
		lbl_name.setHorizontalAlignment(SwingConstants.CENTER);
		panel_3.add(lbl_name);
		
		JLabel lbl_age = new JLabel("-");
		sl_panel_3.putConstraint(SpringLayout.NORTH, lbl_age, 4, SpringLayout.NORTH, lbl_tage);
		sl_panel_3.putConstraint(SpringLayout.WEST, lbl_age, 34, SpringLayout.EAST, lbl_tage);
		sl_panel_3.putConstraint(SpringLayout.EAST, lbl_age, -10, SpringLayout.EAST, panel_3);
		lbl_age.setFont(new Font("동그라미재단M", Font.PLAIN, 15));
		lbl_age.setHorizontalAlignment(SwingConstants.CENTER);
		panel_3.add(lbl_age);
		
		JLabel lbl_tel = new JLabel("-");
		sl_panel_3.putConstraint(SpringLayout.NORTH, lbl_tel, 43, SpringLayout.SOUTH, lbl_age);
		sl_panel_3.putConstraint(SpringLayout.EAST, lbl_tel, -10, SpringLayout.EAST, panel_3);
		lbl_tel.setFont(new Font("동그라미재단M", Font.PLAIN, 15));
		sl_panel_3.putConstraint(SpringLayout.WEST, lbl_tel, 6, SpringLayout.EAST, lbl_ttel);
		lbl_tel.setHorizontalAlignment(SwingConstants.CENTER);
		panel_3.add(lbl_tel);
		springLayout.putConstraint(SpringLayout.EAST, panel_2, 422, SpringLayout.WEST, frame.getContentPane());
		frame.getContentPane().add(panel_2);
		SpringLayout sl_panel_2 = new SpringLayout();
		panel_2.setLayout(sl_panel_2);
		
		JButton btn_add = new JButton("\uC5F0\uB77D\uCC98 \uCD94\uAC00");
		btn_add.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				window.frame.setVisible(false);
				
				dialog = new PhoneDialog();
				dialog.setVisible(true);
											
			}
		});
		btn_add.setFont(new Font("동그라미재단M", Font.PLAIN, 15));
		sl_panel_2.putConstraint(SpringLayout.WEST, btn_add, 51, SpringLayout.WEST, panel_2);
		sl_panel_2.putConstraint(SpringLayout.SOUTH, btn_add, -10, SpringLayout.SOUTH, panel_2);
		panel_2.add(btn_add);
		
		JButton btn_delete = new JButton("\uC5F0\uB77D\uCC98 \uC0AD\uC81C");
		btn_delete.setFont(new Font("동그라미재단M", Font.PLAIN, 15));
		sl_panel_2.putConstraint(SpringLayout.SOUTH, btn_delete, 0, SpringLayout.SOUTH, btn_add);
		sl_panel_2.putConstraint(SpringLayout.EAST, btn_delete, -46, SpringLayout.EAST, panel_2);
		panel_2.add(btn_delete);
	}
}
