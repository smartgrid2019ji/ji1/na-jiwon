package 전화번호부;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class PhoneDAO {
	Connection conn = null;
	PreparedStatement pst = null;
	ArrayList<String> namelist = new ArrayList<>();

	public PhoneDAO() {

		try {

			Class.forName("oracle.jdbc.driver.OracleDriver");
			String url = "jdbc:oracle:thin:@127.0.0.1:1521:xe";
			String dbId = "hr";
			String dbPw = "hr";


			conn = DriverManager.getConnection(url, dbId, dbPw);
			if (conn != null) {
				System.out.println("연결 성공");
			} else {
				System.out.println("연결 실패");
			}

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	
	public ArrayList<String> getlist() {
		
		try {
			String sql = "select name from Phone";
			pst = conn.prepareStatement(sql);
			
			ResultSet rs = pst.executeQuery();
			while(rs.next()) {
				namelist.add(rs.getString(1));	
			}
			return namelist;
			
			
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}



}
